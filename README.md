# rm_dbus

#### 介绍
本程序用于RoboMaster当中DT7遥控器dbus协议解码，基于STM32 HAL库开发，仿照HAL库代码风格，实现简单易于迁移的数据接口

#### 软件架构
本程序主要分三个部分
- 初始化 dbus_init
- 解码 dbus_decode
- 回调 dbus_callback

对外接口为dbus_ctrl_data
采用topic模式，在整个项目中，任何文件#include "dbus.h"即可直接访问dbus的信息

#### 安装教程

1.  本程序的只需要将.c和.h文件按照自己的风格放置在工作文件夹下即可

#### 使用说明

1.  在CubeMX中配置正确的串口和中断，需要打开串口全局中断和DMA接收
2.  在初始化中调用dbus_init，需要填入两个参数：串口handle和DMA handle，注意DMA handle在CubeMX生成的文件中并没有extern声明，需要初始化时自行声明，这里使用当然不符合面向对象精神。本程序中使用DMA handle的目的是判断数据包的完整性，如果不需要判断完整性也可以删除这部分相关程序
3.  在串口中断处理函数中插入dbus_callback，程序会自动完成接收并解码
4.  本程序唯一的对外接口是dbus_ctrl_data，可以通过dbus_ctrl_data.的方式在工程任何地方获取dbus传来的当前信息
5.  本程序在使用独立看门狗实现关控保护功能，如果需要开启，需要解除在.h文件中#define DBUS_IWDG的注释

#### 其他说明

由于遥控器固件，键盘q e shift ctrl的键位与说明书不符，请以实际测试结果为准